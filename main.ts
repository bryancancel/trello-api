import fetch from 'node-fetch';
import https from 'https';
import yargs from 'yargs';

const key = process.env.TRELLO_API_KEY || 'key';
const token = process.env.TRELLO_API_TOKEN || 'token';
const argv = getYargs();
const baseUrl = 'https://api.trello.com';

/*

https://developer.atlassian.com/cloud/trello/rest/

*/
async function main() {
    const cmdUrl = getCmdUrl(argv._, argv.cardId, argv.boardId, argv.listId);

    const { response, text } = await fetchTrello(cmdUrl);

    if (response.status === 200) {
        const formattedText = await getFormattedText(text, argv._);

        console.log(formattedText);
    };
}

async function fetchTrello(cmdUrl: string) {
    // TODO: remove temp bypass of SELF_SIGNED_CERT_IN_CHAIN error
    const agent = new https.Agent({
        rejectUnauthorized: false
    });

    const response = await fetch(baseUrl + cmdUrl + '?key=' + key + '&token=' + token, {
        method: 'GET',
        headers: {
            'Accept': 'application/json'
        },
        agent
    });

    const text = await response.text();

    console.log(
        `${response.status} ${response.statusText}\n\n` +
        `${text}\n`
    );

    return { response, text };
}

async function fetchTrelloChained(cmdUrl: string) {
    // TODO: remove temp bypass of SELF_SIGNED_CERT_IN_CHAIN error
    const agent = new https.Agent({
        rejectUnauthorized: false
    });

    const response = await fetch(baseUrl + cmdUrl + '?key=' + key + '&token=' + token, {
        method: 'GET',
        headers: {
            'Accept': 'application/json'
        },
        agent
    });

    const text = await response.text();

    return { response, text };
}

function getCmdUrl(cmd: any, cardId?: string, boardId?: string, listId?: string) {
    let cmdUrl = '';
    if (cmd.toString() === 'me') {
        cmdUrl = '/1/members/me/';
    } else if (cmd.toString() === 'attachments' || cmd.toString() === 'attachments-status') {
        cmdUrl = `/1/cards/${cardId}/attachments`;
    } else if (cmd.toString() === 'board') {
        cmdUrl = `/1/boards/${boardId}`;
    } else if (cmd.toString() === 'card') {
        cmdUrl = `/1/cards/${cardId}`;
    } else if (cmd.toString() === 'list') {
        cmdUrl = `/1/lists/${listId}`;
    };
    return cmdUrl;
}

async function getFormattedText(rawtext: string, cmd: any) {
    const parsedText = JSON.parse(rawtext);
    let formattedText = '';

    if (cmd.toString() === 'me') {
        formattedText =
            `id,` +
            `fullName\n`;

        formattedText +=
            `${parsedText.id},` +
            `${parsedText.fullName}\n`;
    } else if (cmd.toString() === 'attachments') {
        formattedText =
            `id,` +
            `url\n`;

        parsedText.forEach((attachment: { id: string; url: string; }) => {
            formattedText +=
                `${attachment.id},` +
                `${attachment.url}\n`;
        });
    } else if (cmd.toString() === 'attachments-status') {
        // attachment header
        formattedText =
            `\"id\",` +
            `\"url\",` +
            `\"cardId\",` +
            `\"shortLink\",` +
            `\"name\",` +
            `\"idBoard\",` +
            `\"idList\",` +
            `\"listName\"\n`;
        let parsedCardId = '';
        const indexOfCardId = 4;

        for (const attachment of parsedText) {
            // attachment info
            formattedText +=
                `\"${attachment.id}\",` +
                `\"${attachment.url}\",`;

            // card info
            parsedCardId = attachment.url.split('/')[indexOfCardId];
            formattedText +=
                `\"${parsedCardId}\",`;

            const cmdUrlCardId = getCmdUrl('card', parsedCardId);

            const cardInfo = await fetchTrelloChained(cmdUrlCardId);

            if (cardInfo.response.status === 200) {
                const parsedTextCardId = JSON.parse(cardInfo.text);

                formattedText +=
                    `\"${parsedTextCardId.shortLink}\",` +
                    `\"${parsedTextCardId.name}\",` +
                    `\"${parsedTextCardId.idBoard}\",` +
                    `\"${parsedTextCardId.idList}\",`;

                // save it to the current attachment to be used in list api call
                attachment.idList = parsedTextCardId.idList
            } else {
                console.log(
                    `request: ${cmdUrlCardId}\n` +
                    `response: ${cardInfo.response.status} ${cardInfo.response.statusText}\n` +
                    `text: ${cardInfo.text}\n` +
                    `attachment-url: ${attachment.url}\n`
                );

                formattedText +=
                    `\"\",` +
                    `\"\",` +
                    `\"\",` +
                    `\"\",`;
            }

            // list info
            const cmdUrlListId = getCmdUrl('list', '', '', attachment.idList);

            const listInfo = await fetchTrelloChained(cmdUrlListId);

            if (listInfo.response.status === 200) {
                const parsedTextListId = JSON.parse(listInfo.text);

                formattedText +=
                    `\"${parsedTextListId.name}\"\n`;
            } else {
                console.log(
                    `request: ${cmdUrlListId}\n` +
                    `response: ${listInfo.response.status} ${listInfo.response.statusText}\n` +
                    `text: ${listInfo.text}\n` +
                    `attachment-url: ${attachment.url}\n`
                );

                formattedText +=
                    `\"\"\n`;
            };
        };

        // unique format
        console.log('unique-format:')

        const arrayFormattedText = formattedText.toString().split('\n');
        const delimiter = '\",\"';
        const indexId = 0;
        const indexList = 7;
        const indexUrl = 1

        // sort by list name
        arrayFormattedText.sort((a, b) => {
            const colsa = a.toString().split(delimiter);
            const colsb = b.toString().split(delimiter);

            const namea = colsa[indexList]; // TODO: ignore upper and lowercase
            const nameb = colsb[indexList]; // TODO: ignore upper and lowercase

            if (namea < nameb) {
                return -1;
            }
            if (namea > nameb) {
                return 1;
            }

            // names must be equal
            return 0;
        });

        arrayFormattedText.forEach(row => {
            const columns = row.toString().split(delimiter);

            if (columns && columns[indexId] !== '') {
                const listName = columns[indexList].trim().replace('\"', '');
                if (listName !== '') {
                    const attachmentStatus = listName.toLowerCase().includes('done') ? 'x' : ' '; // if done the prefix status [x] else mark [ ]
                    const attachmentUrl = columns[indexUrl];

                    if (listName.toLowerCase().includes('priorities') ||
                        listName.toLowerCase().includes('goals') ||
                        listName.toLowerCase().includes('listname')) {
                        // if a header, priority, or goal then skip
                    } else {
                        console.log(
                            `[${attachmentStatus}] ` +
                            `${attachmentUrl}`
                        )
                    }
                }
            };
        });

        console.log('\n');
    } else if (cmd.toString() === 'board') {
        formattedText =
            `id,` +
            `name\n`;

        formattedText +=
            `${parsedText.id},` +
            `${parsedText.name}\n`;
    } else if (cmd.toString() === 'card') {
        formattedText =
            `shortLink,` +
            `name,` +
            `idBoard,` +
            `idList\n`;

        formattedText +=
            `${parsedText.shortLink},` +
            `${parsedText.name},` +
            `${parsedText.idBoard},` +
            `${parsedText.idList}\n`;
    } else if (cmd.toString() === 'list') {
        formattedText =
            `id,` +
            `name\n`;

        formattedText +=
            `${parsedText.id},` +
            `${parsedText.name}\n`;
    };
    return formattedText;
}

function getYargs() {
    return yargs
        .usage('Usage: $0 <command> [options]')
        .demandCommand(1, 'You need at least one command before moving on')
        .command('me', 'Get member info for me')
        .command('attachments', 'Get attachments for a given Card ID')
        .command('attachments-status', 'Get attachments statuses for a given Card ID')
        .command('board', 'Get info for a given Board ID')
        .command('card', 'Get info for a given Card ID')
        .command('list', 'Get info for a given List ID')
        .example('$0 npm start -- attachments -c {cardId}', 'Get attachments for a given Card ID')
        .example('$0 npm start -- attachments-status -c {cardId}', 'Get attachments statuses for a given Card ID')
        .example('$0 npm start -- board -b {boardId}', 'Get info for a given Board ID')
        .example('$0 npm start -- card -c {cardId}', 'Get info for a given Card ID')
        .example('$0 npm start -- list -l {listId}', 'Get info for a given List ID')
        .example('$0 npm start -- me', 'Get member info for me')
        .option('boardId', {
            alias: 'b',
            type: 'string',
            description: 'The ID of the Board'
        })
        .option('cardId', {
            alias: 'c',
            type: 'string',
            description: 'The ID of the Card'
        })
        .option('listId', {
            alias: 'l',
            type: 'string',
            description: 'The ID of the List'
        })
        .wrap(null)
        .help('h')
        .alias('h', 'help').argv;
}

main();