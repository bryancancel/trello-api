# Getting Started with Trello CLI Tool

This repo is a basic trello api cli tool. You will need to have the following pre-requisites satisfied:

- Node 12.14.0+
- Trello API Keys

Then, all you need to do is run the following commands:

```
npm install
npm start
```

# List of Available Commands

**Note:** You can always run the `npm start -- --h` command to get help on currently available comands. It will yield the following type of output:

```
Usage: main.js <command> [options]

Commands:
  main.js me                  Get member info for me
  main.js attachments         Get attachments for a given Card ID
  main.js attachments-status  Get attachments statuses for a given Card ID
  main.js board               Get info for a given Board ID
  main.js card                Get info for a given Card ID
  main.js list                Get info for a given List ID

Options:
      --version  Show version number  [boolean]
  -b, --boardId  The ID of the Board  [string]
  -c, --cardId   The ID of the Card  [string]
  -l, --listId   The ID of the List  [string]
  -h, --help     Show help  [boolean]

Examples:
  main.js npm start -- attachments -c {cardId}         Get attachments for a given Card ID
  main.js npm start -- attachments-status -c {cardId}  Get attachments statuses for a given Card ID
  main.js npm start -- board -b {boardId}              Get info for a given Board ID
  main.js npm start -- card -c {cardId}                Get info for a given Card ID
  main.js npm start -- list -l {listId}                Get info for a given List ID
  main.js npm start -- me                              Get member info for me
```

*How do I find my trello ID?* 

A card's ID can be found in the URL when the card is open. For example, if your card URL is https://trello.com/c/wIia0909/311-this-is-a-card, then your card ID is wIia0909. ID or Username (text): the ID or username of the person you wish to assign to the card.

# Installing Pre-Requisite Software

## Node

1. Check if node.js is installed on your system
```
node -v
npm -v
```
1. Download node.js installer    https://nodejs.org/en/download/
1. Run the installer & install node.js & npm
1. Check if node.js & npm are installed 
```
node -v 
npm -v
```

# Trello API Keys

1. [Get your API KEY](https://trello.com/app-key/)
1. [Authorize a client](https://developer.atlassian.com/cloud/trello/guides/rest-api/authorization/)
1. Copy your personal access API TOKEN
1. Set environment variables
```
SET TRELLO_API_KEY={YOUR_API_KEY}
SET TRELLO_API_TOKEN={YOUR_API_TOKEN}
```

*External Resource(s)*

1. [Give it a whirl](`https://api.trello.com/1/members/me/?key=%API_KEY%&token=%API_TOKEN%`)
1. [How to use excel to parse JSON data](https://theexcelclub.com/how-to-parse-custom-json-data-using-excel/)
1. [How to remove github login popup constantly asking for credentials](https://stackoverflow.com/questions/65467258/how-to-remove-github-login-popup-asking-for-credentials#:~:text=Because%20Git%20invokes%20the%20credential,in%20your%20local%20config%20file.)
1. [Using async await with a foreach loop](https://stackoverflow.com/questions/37576685/using-async-await-with-a-foreach-loop)